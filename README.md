
# GovBook

## Start the server
### Backend server

     mvn --projects backend spring-boot:run


## Get an access token to comunicate with the api

    {server}/oauth/token?grant_type=password&username={username}&password={password}
	+
	Authorization header with username 'my-trusted-client' and password '{password}'