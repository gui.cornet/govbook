package com.govbook.app.repositories;

import com.govbook.app.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    List<Article> findAll();

    List<Article> findByUserId(Long id);

}
