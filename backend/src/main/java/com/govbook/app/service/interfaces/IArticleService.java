package com.govbook.app.service.interfaces;

import com.govbook.app.dto.ArticleDTO;
import com.govbook.app.service.DataServiceException;

import java.util.List;

public interface IArticleService {
    ArticleDTO publish(ArticleDTO articleDTO) throws DataServiceException;

    List<ArticleDTO> articlesByUsername(String username) throws DataServiceException;
}
