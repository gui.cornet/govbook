package com.govbook.app.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbastractService<T, R> {

    @Autowired
    protected ModelMapper modelMapper;

    public abstract T convertToEntity(R dto);

    public abstract R convertToDto(T dto);

    protected List<T> convertToEntities(List<R> dtos){
        return dtos.stream()
                .map(this::convertToEntity)
                .collect(Collectors.toList());
    }

    protected List<R> convertToDtos(List<T> entities){
        return entities.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

}
