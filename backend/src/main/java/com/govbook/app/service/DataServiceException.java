package com.govbook.app.service;

public class DataServiceException extends Exception {

    public DataServiceException(String msg, String... params) {
        super(String.format(msg, params));
    }
}
