package com.govbook.app.service;

import com.govbook.app.dto.UserDTO;
import com.govbook.app.dto.UserRegistrationDTO;
import com.govbook.app.entities.Role;
import com.govbook.app.entities.User;
import com.govbook.app.repositories.RoleRepository;
import com.govbook.app.repositories.UserRepository;
import com.govbook.app.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserService extends AbastractService<User, UserDTO> implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public Iterable<User> findAll(){
        return userRepository.findAll();
    }

    @Override
    public User findByEmail(String email){

        return userRepository.findByEmail(email)
                .orElse(new User());
    }

    @Override
    public boolean existsByEmail(String email){
        return userRepository.existsByEmail(email);
    }

    @Override
    public User convertToEntity(UserDTO dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDTO convertToDto(User entity) {
        return modelMapper.map(entity, UserDTO.class);
    }

    @Override
    public UserDTO save(UserRegistrationDTO userRegistration) throws DataServiceException {
        User user = new User();
        user.setEmail(userRegistration.getEmail());
        user.setPassword(passwordEncoder().encode(userRegistration.getPassword()));

        Role userRole = roleRepository.findByRole(Role.role.USER.name().toLowerCase())
                .orElseThrow(() -> new DataServiceException("Role User non trouvé"));

        user.setRoles(new HashSet<>(Arrays.asList(userRole)));

        User savedUser = userRepository.save(user);
        return convertToDto(savedUser);
    }
}
