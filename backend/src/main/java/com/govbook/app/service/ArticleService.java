package com.govbook.app.service;

import com.govbook.app.dto.ArticleDTO;
import com.govbook.app.entities.Article;
import com.govbook.app.entities.User;
import com.govbook.app.repositories.ArticleRepository;
import com.govbook.app.service.interfaces.IArticleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class ArticleService extends AbastractService<Article, ArticleDTO> implements IArticleService {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserService userService;

    public List<Article> findAll(){
        return articleRepository.findAll();
    }

    @Override
    public ArticleDTO publish(ArticleDTO articleDTO) throws DataServiceException {
        Article article = this.convertToEntity(articleDTO);
        article.setCreatedDate(LocalDateTime.now());
        return this.convertToDto(articleRepository.save(article));
    }

    @Override
    public List<ArticleDTO> articlesByUsername(String username) throws DataServiceException {
        User user = userService.findByEmail(username);

        if(user.getId() == null){
            throw new DataServiceException("username %s not found", username);
        }

        List<Article> articles = articleRepository.findByUserId(user.getId());
        List<ArticleDTO> articlesDTO = this.convertToDtos(articles);
        return articlesDTO;
    }

    @Override
    public Article convertToEntity(ArticleDTO dto){
        Article article = modelMapper.map(dto, Article.class);
        article.setUser(userService.findByEmail(dto.getUserEmail()));
        return article;
    }

    @Override
    public ArticleDTO convertToDto(Article entity){
        ArticleDTO dto = modelMapper.map(entity, ArticleDTO.class);
        dto.setUserEmail(entity.getUser().getEmail());
        return dto;
    }
}
