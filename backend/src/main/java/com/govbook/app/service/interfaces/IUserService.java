package com.govbook.app.service.interfaces;

import com.govbook.app.dto.UserDTO;
import com.govbook.app.dto.UserRegistrationDTO;
import com.govbook.app.entities.User;
import com.govbook.app.service.DataServiceException;

public interface IUserService {
    Iterable<User> findAll();

    User findByEmail(String email);

    boolean existsByEmail(String email);

    UserDTO save(UserRegistrationDTO userRegistration) throws DataServiceException;
}
