package com.govbook.app.dto;

import java.util.Objects;

public class UserRegistrationDTO {
    private String email;
    private String password;
    private String passwordConfirmation;

    public UserRegistrationDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public boolean isPasswordsEquals(){
        return Objects.equals(password, passwordConfirmation);
    }
}
