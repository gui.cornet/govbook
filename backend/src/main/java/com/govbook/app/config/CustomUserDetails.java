package com.govbook.app.config;

import com.govbook.app.entities.Role;
import com.govbook.app.entities.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;
import java.util.stream.Collectors;

public class CustomUserDetails implements UserDetails {

    public static final String ROLE_PREFIX = "ROLE_";
    private String password;
    private String username;
    private Set<? extends GrantedAuthority> authorities;

    public CustomUserDetails(String password, String username, Set<? extends GrantedAuthority> authorities) {
        this.password = password;
        this.username = username;
        this.authorities = authorities;
    }

    public CustomUserDetails(User user) {
        this.password = user.getPassword();
        this.username = user.getEmail();
        this.authorities = getAuthoritiesFromRoles(user.getRoles());
    }

    public Set<GrantedAuthority> getAuthoritiesFromRoles(Set<Role> roles){
        return roles.stream()
                .map(Role::getRole)
                .map(String::toUpperCase)
                .map(ROLE_PREFIX::concat)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Set<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
