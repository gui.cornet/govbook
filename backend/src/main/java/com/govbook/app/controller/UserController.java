package com.govbook.app.controller;


import com.govbook.app.dto.UserRegistrationDTO;
import com.govbook.app.service.DataServiceException;
import com.govbook.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;

    @PutMapping(value = "/register")
    public String register(@RequestBody UserRegistrationDTO userRegistration) throws DataServiceException {
        if(!userRegistration.isPasswordsEquals()){
            return "Password do not match";
        }
        if(userService.existsByEmail(userRegistration.getEmail())){
            return "Email already exists";
        }

        userService.save(userRegistration);

        return "User registered";
    }

}
