package com.govbook.app.controller;


import com.govbook.app.config.CustomUserDetails;
import com.govbook.app.dto.ArticleDTO;
import com.govbook.app.entities.Article;
import com.govbook.app.service.ArticleService;
import com.govbook.app.service.DataServiceException;
import com.govbook.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api")
public class ArticleController {

    @Autowired
    ArticleService articleService;
    @Autowired
    UserService userService;

    @GetMapping(value="/articles")
    public List<Article> posts(){
        return articleService.findAll();
    }

    @PutMapping(value = "/article")
    public String addArticles(@RequestBody ArticleDTO article) throws DataServiceException {
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        article.setUserEmail(userDetails.getUsername());
        articleService.publish(article);
        return "Article was published";
    }

    @GetMapping(value = "/articles/{username}")
    public List<ArticleDTO> articlesByUSername (@PathVariable String username) throws DataServiceException {
        List<ArticleDTO> articles = articleService.articlesByUsername(username);
        return articles;
    }


}
