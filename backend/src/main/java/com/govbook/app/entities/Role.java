package com.govbook.app.entities;

import javax.persistence.*;

@Entity(name = "role")
public class Role {

    public enum role {
        USER, ADMIN;

        public Role newRole(){
            return new Role(this.name());
        }
    }

    public Role(String role) {
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") private Long id;

    @Column(name = "role", nullable = false) private String role;

    public Role() {}

    public long getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
